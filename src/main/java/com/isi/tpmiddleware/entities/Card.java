package com.isi.tpmiddleware.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@SequenceGenerator(name="seq1", initialValue=1000)
public class Card {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq1")
    private Long cardNumber;
    private Date expDate;
    private int cvv;

    @ManyToOne(cascade = {CascadeType.PERSIST,CascadeType.REFRESH})
    private Account account;

    @Column(columnDefinition = "boolean default true")
    private boolean status;
}
