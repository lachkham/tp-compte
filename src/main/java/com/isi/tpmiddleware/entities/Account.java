package com.isi.tpmiddleware.entities

        ;


import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Null;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@SequenceGenerator(name="seq", initialValue=1000000000)
public class Account implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    private Long accountNumber;
    private double balance;


    @ManyToOne(cascade = {CascadeType.PERSIST,CascadeType.DETACH,CascadeType.MERGE,CascadeType.REFRESH})
    private Client owner;




    @Column(columnDefinition = "boolean default true")
    private boolean status;



}
