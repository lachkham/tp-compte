package com.isi.tpmiddleware.entities;


import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;
import javax.validation.constraints.Email;

@Entity
@Data
public class Client implements Serializable {

    @Id
    private String cin ;
    private String name;
    private String lastName;
    private Date birthDate;
    @Email(message = "Email should be valid")
    private String email;




}
