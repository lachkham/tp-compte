package com.isi.tpmiddleware.repository;

import com.isi.tpmiddleware.entities.Card;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CardRepository extends JpaRepository<Card,Long> {
    List<Card> findAllByAccount_AccountNumber(Long accountNumber);
    List<Card> findAllByAccount_Owner_Cin(String cin);

}
