package com.isi.tpmiddleware.controllers;


import com.isi.tpmiddleware.entities.Card;
import com.isi.tpmiddleware.repository.CardRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/v1/Card")
@CrossOrigin(origins = "*")
public class CardController {

    private final CardRepository cardRepository;


    public CardController(CardRepository cardRepository) {
        this.cardRepository = cardRepository;
    }


    @GetMapping("/")
    public ResponseEntity findAll(){
        return ResponseEntity.ok(cardRepository.findAll());
    }

    @GetMapping("/{cardNumber}")
    public ResponseEntity findCardById(@PathVariable(name = "cardNumber") Long cardNumber){
        if(cardNumber==null){
            return ResponseEntity.badRequest().body("Empty parameter");
        }

        Optional<Card> card = cardRepository.findById(cardNumber);

        if(card.isPresent()){
            return ResponseEntity.ok(card);
        }else{
            return  ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/")
    public ResponseEntity createCard (@RequestBody Card cardBody){
        if (cardBody==null){
            return  ResponseEntity.badRequest().body("Empty Request Body");
        }

        Optional<Card> card= cardRepository.findById(cardBody.getCardNumber());
        if (!card.isPresent()){
            Card createCard = cardRepository.save(cardBody);
            return ResponseEntity.ok(createCard);
        }
        return ResponseEntity.badRequest().body("Account exist");
    }

    @DeleteMapping("/{cardNumber}")
    public  ResponseEntity deleteCard(@PathVariable(name = "cardNumber") Long cardNumber){
        if (cardNumber ==  null){
            return ResponseEntity.badRequest().body("Empty parameter");
        }

        Optional<Card> card= cardRepository.findById(cardNumber);

        if (card.isPresent()){
            cardRepository.deleteById(cardNumber);
            return ResponseEntity.ok().body(card);
        }
        return ResponseEntity.notFound().build();

    }

    @PutMapping("/")
    public ResponseEntity UpdateCard (@RequestBody Card cardBody){
        if (cardBody==null){
            return  ResponseEntity.badRequest().body("Empty Request Body");
        }

        Optional<Card> card= cardRepository.findById(cardBody.getCardNumber());
        if (card.isPresent()){
        Card createCard = cardRepository.save(cardBody);
        return ResponseEntity.ok(createCard);
        }
        return ResponseEntity.notFound().build();

    }
    @PatchMapping("/")
    public ResponseEntity PatchCard (@RequestBody Card cardBody) {
        return ResponseEntity.ok(cardBody);
    }
    

    }
