package com.isi.tpmiddleware.controllers;


import com.isi.tpmiddleware.entities.Account;
import com.isi.tpmiddleware.repository.AccountRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/v1/Account")
@CrossOrigin(origins = "*")
public class AccountController {
    private final AccountRepository accountRepository;


    public AccountController(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @GetMapping("/")
    public ResponseEntity findAll(){
        return ResponseEntity.ok(accountRepository.findAll());
    }

    @GetMapping("/{accountNumber}")
    public ResponseEntity findAccountById(@PathVariable(name = "accountNumber") Long accountNumber){
            if(accountNumber==null){
                return ResponseEntity.badRequest().body("Empty parameter");
            }

        Optional<Account> account = accountRepository.findById(accountNumber);

            if(account.isPresent()){
                return ResponseEntity.ok(account);
            }else{
                return  ResponseEntity.notFound().build();
            }
    }

    @PostMapping("/")
    public ResponseEntity createAccount (@RequestBody Account accountBody){
        if (accountBody==null){
            return  ResponseEntity.badRequest().body("Empty Request Body");
        }

        Optional<Account> account= accountRepository.findById(accountBody.getAccountNumber());

        if (!account.isPresent()){
            Account createAccount = accountRepository.save(accountBody);
            return ResponseEntity.ok(createAccount);
        }
        return ResponseEntity.badRequest().body("Account exist");
    }

    @DeleteMapping("/{accountNumber}")
    public  ResponseEntity deleteaccount (@PathVariable(name = "accountNumber") Long accountNumber){
        if (accountNumber ==  null){
            return ResponseEntity.badRequest().body("Empty parameter");
        }

        Optional<Account> account= accountRepository.findById(accountNumber);

        if (account.isPresent()){
            accountRepository.deleteById(accountNumber);
            return ResponseEntity.ok().body(account);
        }
        return ResponseEntity.notFound().build();

    }

    @PutMapping("/")
    public ResponseEntity updateAccount (@RequestBody Account accountBody){
        if (accountBody==null){
            return  ResponseEntity.badRequest().body("Empty Request Body");
        }


        Optional<Account> account= accountRepository.findById(accountBody.getAccountNumber());

        if (account.isPresent()){
            Account createAccount = accountRepository.save(accountBody);
            return ResponseEntity.ok(createAccount);
        }
        return ResponseEntity.notFound().build();




    }

}
