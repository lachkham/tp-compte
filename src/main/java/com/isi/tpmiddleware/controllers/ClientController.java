package com.isi.tpmiddleware.controllers;

import com.isi.tpmiddleware.entities.Client;
import com.isi.tpmiddleware.repository.ClientRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/v1/Client")
@CrossOrigin(origins = "*")
public class ClientController {
    private final ClientRepository clientRepository;


    public ClientController(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }
    @GetMapping("/")
    public ResponseEntity findAll(){
        return ResponseEntity.ok(clientRepository.findAll());
    }

    @GetMapping("/{cin}")
    public ResponseEntity findClientById(@PathVariable(name = "cin") String cin){
        if(cin==null){
            return ResponseEntity.badRequest().body("Empty parameter");
        }

        Optional<Client> client = clientRepository.findById(cin);

        if(client.isPresent()){
            return ResponseEntity.ok(client);
        }else{
            return  ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/")
    public ResponseEntity createClient (@RequestBody Client clientBody){
        if (clientBody==null){
            return  ResponseEntity.badRequest().body("Empty Request Body");
        }

        Optional<Client> client= clientRepository.findById(clientBody.getCin());

        if (!client.isPresent()){
            Client createClient = clientRepository.save(clientBody);
            return ResponseEntity.ok(createClient);
        }
        return ResponseEntity.badRequest().body("client exist");
    }

    @DeleteMapping("/{cin}")
    public  ResponseEntity deleteClient (@PathVariable(name = "cin") String cin){
        if (cin ==  null){
            return ResponseEntity.badRequest().body("Empty parameter");
        }

        Optional<Client> client = clientRepository.findById(cin);

        if (client.isPresent()){
            clientRepository.deleteById(cin);
            return ResponseEntity.ok().body(client);
        }
        return ResponseEntity.notFound().build();

    }

    @PutMapping("/")
    public ResponseEntity updateClient (@RequestBody Client clientBody){
        if (clientBody==null){
            return  ResponseEntity.badRequest().body("Empty Request Body");
        }


        Optional<Client> Client= clientRepository.findById(clientBody.getCin());

        if (Client.isPresent()){
            Client createClient = clientRepository.save(clientBody);
            return ResponseEntity.ok(createClient);
        }
        return ResponseEntity.notFound().build();




    }


}
